﻿using System;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using DLLSharedMethods;
using System.Collections;
using MailMerge.Lib;

namespace StampaSegni
{
    public class PrintEngine
    {
        public string PdfInputFileName;
        public string XmlInputFileName;
        public string PdfOutputFileName;
        public static string SignType;
        private XmlBuilder _xml;
        public void CreaDocumentoConSegniImbustamento()
        {
            //**********FOR ADDING WHITE PAGE IF YOU DON'T FIND BETTER SOLUTION************************
            //string FILE_PAG_BIANCA = Path.Combine(CommonHelper.PathProcess, "ini", "bianca.pdf");
            //FileInfo filepagBianca = new FileInfo(FILE_PAG_BIANCA);
            //if (!filepagBianca.Exists)
            //{
            //    Console.WriteLine(String.Format("File {0} non presente.", FILE_PAG_BIANCA));               
            //    throw new IOException(String.Format("Errore. File \"{0}\" non presente.", FILE_PAG_BIANCA));
            //}
            _xml = XmlBuilder.Builder();

            const float marginedx = 10;
            string imgsignrespect = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "imgsignrespect.gif");
            FileInfo fileImgResp = new FileInfo(imgsignrespect);
            if (!fileImgResp.Exists)
            {
                throw new IOException(String.Format("Errore. File \"{0}\" non presente.", imgsignrespect));
            }
            const float larghezzazonasegni = 5;
            const float marginezonasegni = 5;
            const float distanzasegni = 12;
            const int spessoreSegni = 2;
            int inizContatoreBinario = 7; // <------ quando si farà il multithread va rivisto questo parametro 
            const bool fr = true; // <------ se è fronte/retro, per il momento metti false fisso
            ArrayList clear = new ArrayList(); // <------ è sempre vuoto
            Hashtable inserti = new Hashtable(); // <---------- per ora lascia vuoto
            Hashtable fogli = new Hashtable();
            ArrayList chiudiBusta = new ArrayList();
            const float marginesup = 105;
            const float start = 817;

            float[] segni = { 0, 0, 0, 0, 0, 0 }; 
            for (int i = 0; i <= 5; i++)
                segni[i] = start - marginesup - (distanzasegni * i);

            const bool inserisciDtmxImbustamento = true;
            const bool inserisciSegniImbustamento = true;
            using (MemoryStream ms = new MemoryStream())
            {
                Document doc = new Document();
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                writer.PDFXConformance = PdfWriter.PDFA1B;
                writer.CreateXmpMetadata();
                int numTotalePagine = 0;
                doc.Open();
                fogli.Clear();
                PdfReader reader = new PdfReader(PdfInputFileName);
                //PdfReader whitePageReader = new PdfReader(FILE_PAG_BIANCA);
                int numpagine = reader.NumberOfPages;
                numTotalePagine += numpagine;
                if (File.Exists(PdfInputFileName))
                {
                    try
                    {
                        PdfContentByte writ = writer.DirectContent;
                        int inizContaPagina = 1;
                        int inizContaBusta = 1;
                        Pdf pdf = _xml.DeserializeXml(XmlInputFileName);
                        foreach (document t in pdf.document)
                            chiudiBusta.Add(Convert.ToInt32(t.LastPage));
                        for (int pagina = 1; pagina <= numpagine; pagina++)
                        {
                            fogli.Add(pagina, numTotalePagine);
                            int rotazione = reader.GetPageSizeWithRotation(pagina).Rotation;
                            PdfImportedPage page = writer.GetImportedPage(reader, pagina);
                            doc.NewPage();
                            if (rotazione == 90 || rotazione == 270)
                            {
                                const float rotate = 90;
                                float x = PageSize.A4.Width;
                                const float y = 0;
                                const float angle = (float)(-rotate * (Math.PI / 180));
                                float xScale = (float)Math.Cos(angle);
                                float yScale = (float)Math.Cos(angle);
                                float xRot = (float)-Math.Sin(angle);
                                float yRot = (float)Math.Sin(angle);
                                writ.AddTemplate(page, xScale, xRot, yRot, yScale, x, y);
                            }
                            else
                            {
                                writ.AddTemplate(page, 0f, 0f);
                            }
                            SharedMethods._MettiSegniWriter(ref reader, ref  writ, pagina, ref inizContaPagina, chiudiBusta, segni, marginedx, imgsignrespect, larghezzazonasegni, marginezonasegni, distanzasegni, spessoreSegni, ref inizContatoreBinario, fr, ref inizContaBusta, "12345678", inserisciDtmxImbustamento, inserisciSegniImbustamento, ref clear, ref inserti, ref fogli);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
               
                }
                doc.Close();
                string outputFile = Path.Combine(PdfOutputFileName);
                SharedMethods.MemoryStreamToFile(ms, outputFile);
            }
        }
    }
}
