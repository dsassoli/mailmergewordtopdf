﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using iTextSharp.text.pdf;
using Microsoft.Office.Interop.Word;
using Word = Microsoft.Office.Interop.Word;
using Excell = Microsoft.Office.Interop.Excel;
using MailMerge.Lib;
using DataTable = System.Data.DataTable;

namespace MailMergeWordToPDF
{
    public class MailMerge
    {
        #region varDeclarations
        private int _counter;
        private Pdf _pdf;
        private document _doc;
        private SheetChange _sheet;
        private XmlBuilder _xml;
        private int _mergedFileCurrentPageNumber;
        private readonly string _connStr;
        private readonly string _docTemplete;
        private readonly string _fileExcelInput;
        private string DocTemplete
        {
            get
            {
                return _docTemplete;
            }
        }
        private string FileExcelInput
        {
            get
            {
                return _fileExcelInput;
            }
        }
        DataTable _dt;

        public MailMerge(string docTemplate, string fileInputExcel)
        {
            _docTemplete = docTemplate;
            _fileExcelInput = fileInputExcel;
            _mergedFileCurrentPageNumber = 0;
            _connStr = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{0}';Extended Properties=Excel 12.0 Xml", fileInputExcel);
        }
        #endregion

        public void DoMailMerge(string destinationPdfFileName, string destinationXmlFileName)
        {
            _pdf = Pdf.Builder();
            _xml = XmlBuilder.Builder();
            _pdf.document = new List<document>();
            string excelSource = FileExcelInput;
            //OBJECTS OF WORD AND DOCUMENT 
            Application myWordApp = new Application();
            _Document myWordDoc = new Document();
            //OBJECT OF MISSING "NULL VALUE" 
            object oMissing = System.Reflection.Missing.Value;
            //OBJECTS OF FALSE AND TRUE 
            object oTrue = true;
            object oFalse = false;
            //OBJECT OF DOCUMENT PATH
            object fileDocTempl = DocTemplete;

            try
            {
                if (Directory.Exists("C:\\L2Cmailmerge\\"))
                    Directory.Delete("C:\\L2Cmailmerge\\", true);
                Directory.CreateDirectory("C:\\L2Cmailmerge\\");
                //*******************TROVARE NUMERO PAGINE FILE WORD*****************************
                //object missing = System.Reflection.Missing.Value; 
                //Word.WdStatistic stat = Word.WdStatistic.wdStatisticPages;
                //int num = myWordDoc.ComputeStatistics(stat, ref missing);
                myWordDoc = myWordApp.Documents.Add(ref fileDocTempl, ref oMissing, ref oTrue);
                myWordApp.Visible = false;
                Word.MailMerge wrdMailMerge = myWordDoc.MailMerge;
                List<object> listTable = GetTableName();
                const string cmdText = "Select * from [{0}]";
                Object oQuery = string.Format(cmdText, listTable[0]);
                wrdMailMerge.OpenDataSource(excelSource, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing,oMissing, oMissing, oMissing, oMissing, oMissing, oQuery);

                //////// ************************************ IMPORTANTE ****************************
                myWordApp.MailMergeAfterRecordMerge += myWordApp_MailMergeAfterRecordMerge;
                //////// è più veloce mettere una pagina o segni salto pagina senza usare l'evento. Usare l'evento solo per salvare i singoli PDF
                myWordDoc.MailMerge.Execute(ref oFalse);
                string[] files = Directory.GetFiles("C:\\L2Cmailmerge\\");
                using (FileStream fs = new FileStream(destinationPdfFileName, FileMode.Create, FileAccess.Write,FileShare.Read))
                {
                    iTextSharp.text.Document doc1 = new iTextSharp.text.Document();
                    PdfCopy pdfCopy = new PdfCopy(doc1, fs);
                    doc1.Open();
                    foreach (var percorsoFilePdf in files)
                    {
                        PdfReader reader = new PdfReader(percorsoFilePdf);
                        int numpagine = reader.NumberOfPages;
                        for (int I = 1; I < numpagine; I++)
                        {
                            doc1.SetPageSize(reader.GetPageSizeWithRotation(1));
                            PdfImportedPage page = pdfCopy.GetImportedPage(reader, I);
                            pdfCopy.AddPage(page);
                        }
                        //Clean up
                        reader.Close();
                    }
                    //Clean up
                    doc1.Close();
                    //elimina la cartella e tutte le sottocartelle
                    Directory.Delete("C:\\L2Cmailmerge\\", true);
                }
                _xml.SerializeXml(destinationXmlFileName, _pdf);
                
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex);
                throw;
            }
            finally
            {
                Process [] proc = Process.GetProcessesByName("WINWORD");
	            proc[0].Kill();
               // object doNotSaveChanges = WdSaveOptions.wdDoNotSaveChanges;
                //myWordDoc.Close(ref doNotSaveChanges, ref oMissing, ref oMissing);
                //myWordApp.Quit(ref doNotSaveChanges, ref oMissing, ref oMissing);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #region otherFunctions
        private List<object> GetTableName()
        {
            _dt = GetSchema();
            var listTable = (from dr in _dt.AsEnumerable()
                             where dr["TABLE_NAME"].ToString().EndsWith("$")
                             select dr["TABLE_NAME"]).ToList();
            return listTable;
        }

        private DataTable GetSchema()
        {
            OleDbConnection conn = new OleDbConnection(_connStr);
            if (conn.State != ConnectionState.Open) conn.Open();
            DataTable dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            if (conn.State == ConnectionState.Open) conn.Close();

            return dtSchema;
        }
        #endregion
        private void myWordApp_MailMergeAfterRecordMerge(_Document mergeResultDoc)
        {
            Object oMissing = System.Reflection.Missing.Value;
            _counter++;
            string filename = _counter.ToString(CultureInfo.InvariantCulture).PadLeft(8, '0');
            try
            {
                //inserisco le istruzioni nell'xml
                _doc = document.Builder();
                _doc.SheetChange = new List<SheetChange>();
                int numPagesInFile = 0;
                List<Bookmark> orderedResults = mergeResultDoc.Bookmarks.OfType<Bookmark>().OrderBy(d => d.Start).ToList();
                foreach (Bookmark bookMark in orderedResults)
                {//IMPORTANT: THE NAME OF THE BOOKMARK MUST BE CALLED WITH THE PAPER TYPE FOR THAT SECTION 
                    if (bookMark.Name != "ultimaPagina")
                    {
                        _sheet = SheetChange.Builder();
                        _sheet.PageNumber = Convert.ToString(Convert.ToInt32(bookMark.Range.Information[WdInformation.wdActiveEndPageNumber]) + _mergedFileCurrentPageNumber);
                        _sheet.SheetType = bookMark.Name;
                        _doc.SheetChange.Add(_sheet);
                        numPagesInFile++;
                    }
                    else
                        _doc.LastPage = Convert.ToString(_mergedFileCurrentPageNumber + numPagesInFile);
                }
                _doc.StartPage = Convert.ToString(_mergedFileCurrentPageNumber + 1);
                _mergedFileCurrentPageNumber += numPagesInFile;
                _pdf.document.Add(_doc);
                object destinationPdf = string.Format("{0}{1}.pdf", "C:\\L2Cmailmerge\\", filename);
                object wdSaveFormatPdf = WdSaveFormat.wdFormatPDF;
                mergeResultDoc.SaveAs(ref destinationPdf, ref wdSaveFormatPdf, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                                      ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                                      ref oMissing, ref oMissing, ref oMissing);
            }
            catch (Exception)
            {
                object doNotSaveChanges = WdSaveOptions.wdDoNotSaveChanges;
                mergeResultDoc.Close(ref doNotSaveChanges, ref oMissing, ref oMissing);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

        }
    }
}
