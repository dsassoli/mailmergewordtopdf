﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MailMergeWordToPDF
{
    public static class LoggerHelper
    {
        private static string path;

        public static void CreaPathLog(string lotto)
        {
            LoggerHelper.path = CommonHelper.PathProcess + "\\out\\" + lotto + "\\log.txt";
        }


        public static void Log(string message, bool success)
        {
            if (success)
            {
                try
                {

                    using (var sw = File.AppendText(LoggerHelper.path))
                    {
                        sw.Write("******************** " + DateTime.Now);
                        sw.WriteLine(" ********************");
                        sw.WriteLine();
                        sw.WriteLine(message + "\r\n");
                        sw.WriteLine("*************************************************************");
                    }
                }
                catch
                {
                }
            }
        }

        public static void Log(string message)
        {
            try
            {

                using (var sw = File.AppendText(LoggerHelper.path))
                {
                    sw.Write("******************** " + DateTime.Now);
                    sw.WriteLine(" ********************");
                    sw.WriteLine(message + "\r\n\r\n");
                    sw.WriteLine("*************************************************************");
                }


            }
            catch
            {
            }
        }

        public static void Log(Exception exc)
        {
            try
            {
                //using (var sw = File.AppendText(path))
                //{
                //sw.WriteLine(DateTime.Now.ToString() + " :: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                //sw.WriteLine();

                using (StreamWriter sw = new StreamWriter(LoggerHelper.path, true))
                {
                    sw.Write("******************** " + DateTime.Now);
                    sw.WriteLine("********************");
                    if (exc.InnerException != null)
                    {
                        sw.Write("Inner Exception Type: ");
                        sw.WriteLine(exc.InnerException.GetType().ToString());
                        sw.Write("Inner Exception: ");
                        sw.WriteLine(exc.InnerException.Message);
                        sw.Write("Inner Source: ");
                        sw.WriteLine(exc.InnerException.Source);
                        if (exc.InnerException.StackTrace != null)
                            sw.WriteLine("Inner Stack Trace: ");
                        sw.WriteLine(exc.InnerException.StackTrace);
                    }
                    sw.Write("Exception Type: ");
                    sw.WriteLine(exc.GetType().ToString());
                    sw.WriteLine("Exception: " + exc.Message);
                    sw.WriteLine("Stack Trace: ");
                    if (exc.StackTrace != null)
                        sw.WriteLine(exc.StackTrace);
                    sw.WriteLine();
                    sw.WriteLine("*************************************************************");

                }

            }
            //}
            catch
            {
            }
        }

        public static void ClearLog(string _CN)
        {
            CreaPathLog(_CN);

            File.Create(LoggerHelper.path).Close();
        }



        // Log an Exception
        public static void Log(Exception exc, string source)
        {


            using (StreamWriter sw = new StreamWriter(LoggerHelper.path, true))
            {
                sw.Write("******************** " + DateTime.Now);
                sw.WriteLine("********************");
                if (exc.InnerException != null)
                {
                    sw.Write("Inner Exception Type: ");
                    sw.WriteLine(exc.InnerException.GetType().ToString());
                    sw.Write("Inner Exception: ");
                    sw.WriteLine(exc.InnerException.Message);
                    sw.Write("Inner Source: ");
                    sw.WriteLine(exc.InnerException.Source);
                    if (exc.InnerException.StackTrace != null)
                        sw.WriteLine("Inner Stack Trace: ");
                    sw.WriteLine(exc.InnerException.StackTrace);
                }
                sw.Write("Exception Type: ");
                sw.WriteLine(exc.GetType().ToString());
                sw.WriteLine("Exception: " + exc.Message);
                sw.WriteLine("Source: " + source);
                sw.WriteLine("Stack Trace: ");
                if (exc.StackTrace != null)
                    sw.WriteLine(exc.StackTrace);
                sw.WriteLine();
                sw.WriteLine("*************************************************************");

            }

        }
    }
}
