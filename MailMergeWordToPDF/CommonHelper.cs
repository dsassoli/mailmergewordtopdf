﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace MailMergeWordToPDF
{
    public static class CommonHelper
    {


        private static string _pathProcess = null;
        public static string PathProcess
        {
            get
            {
                if (_pathProcess != null)
                    return _pathProcess;

                try
                {

                    _pathProcess = ConfigurationManager.AppSettings["PathAppoggio"].ToString();
                   //_pathProcess = System.IO.Path.Combine(dirAppoggio, "StampaUnione");

                }
                catch (Exception ex)
                {  // se la chiave non esiste...
                    Console.WriteLine("Errore di:" + ex.Message.ToString());
                }

                //System.Environment.CurrentDirectory.ToString();



                return _pathProcess;
            }
        }
    }
}
