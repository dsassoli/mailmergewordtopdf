﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MailMergeWordToPDF;
using StampaSegni;

namespace MailMergeUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MergePanel.Hide();
            SignsPanel.Hide();
            PrintPanel.Hide();
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                printerNameBox.Items.Add(printer);
        }
        #region standards
        private void DoMailMerge()
        {
            try
            {
                MailMerge mm = new MailMerge(TemplateFileBox.Text, ExcelFileBox.Text);
                mm.DoMailMerge(destinationPDFFileName.Text, DestinationXMLFileName.Text);
                MessageBox.Show("Mail merge eseguita correttamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void PrintSigns()
        {
            try
            {
                PrintEngine engine = new PrintEngine
                {
                    XmlInputFileName = DestinationXMLFileName.Text,
                    PdfInputFileName = destinationPDFFileName.Text,
                    PdfOutputFileName = signsPDFFileName.Text
                };
                engine.CreaDocumentoConSegniImbustamento();
                MessageBox.Show("Stampa segni eseguita correttamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void MergeWithSignsButton(object sender, EventArgs e)
        {
            if (MergePanel.Visible)
            {
                List<TextBox> toBeCompiled = new List<TextBox>{TemplateFileBox, ExcelFileBox, DestinationXMLFileName, destinationPDFFileName};
                if(AreFieldsCompiled(toBeCompiled))
                DoMailMerge();
            }
            if (SignsPanel.Visible)
            {
                List<TextBox> toBeCompiled = new List<TextBox> { TemplateFileBox, ExcelFileBox, SignsXLFileName, signsPDFFileName };
                if (AreFieldsCompiled(toBeCompiled))
                    PrintSigns();
            }
            if (PrintPanel.Visible)
            {
                List<TextBox> toBeCompiled = new List<TextBox> {FileToPrint, ToPRintXml};
                if (AreFieldsCompiled(toBeCompiled))
                    PrintSigns();
            }
        }
        private void TemplateFileButton(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Word Files|*.doc;*.docx";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                TemplateFileBox.Text = openFileDialog1.FileName;
        }
        private void ExcelFileButton(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                ExcelFileBox.Text = openFileDialog1.FileName;
        }
        #endregion

        private bool AreFieldsCompiled(IEnumerable<TextBox> toBeCompiled)
        {
            foreach (TextBox field in toBeCompiled.Where(field => String.IsNullOrEmpty(field.Text)))
            {
                fieldRequired.SetError(field, "Inserisci un valore per i campi obbligatori");
                return false;
            }
            return true;
        }

        private void buttonXmlMerge_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "XML Files|*.xml";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                DestinationXMLFileName.Text = saveFileDialog1.FileName;
                fieldRequired.Clear();
            }
        }

        private void buttonPDFMerge_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "PDF Files|*.pdf";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                destinationPDFFileName.Text = saveFileDialog1.FileName;
                fieldRequired.Clear();
            }
        }

        private void buttonPDFSigns_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "PDF Files|*.pdf";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                signsPDFFileName.Text = saveFileDialog1.FileName;
                fieldRequired.Clear();
            }
        }

        private void buttonXLFile_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel Files|*.xlsx";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SignsXLFileName.Text = saveFileDialog1.FileName;
            }
        }

        private void MailMergeMenu_Click(object sender, EventArgs e)
        {
            MergePanel.Show();
        }

        private void PrintSignsMenu_Click(object sender, EventArgs e)
        {
            comboBoxSignType.Items.Add("1 Solo Fronte");
            comboBoxSignType.Items.Add("1 Fronte Retro");
            comboBoxSignType.Items.Add("1 Solo Fronte + 1 Fronte Retro");
            SignsPanel.Show();
        }

        private void TemplateFileBox_TextChanged(object sender, EventArgs e)
        {
            fieldRequired.Clear();
        }

        private void ExcelFileBox_TextChanged(object sender, EventArgs e)
        {
            fieldRequired.Clear();
        }

        private void PrintMenu_Click(object sender, EventArgs e)
        {
            PrintPanel.Show();
        }

        private void configuraNuovaStampanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormConfiguraStampante settingsForm=new FormConfiguraStampante();
            settingsForm.Show();
        }
    }
}
