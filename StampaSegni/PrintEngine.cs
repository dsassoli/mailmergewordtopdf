﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using DLLSharedMethods;
using System.Collections;

namespace StampaSegni
{
    public class PrintEngine
    {
        public void CreaDocumentoConSegniImbustamento(string _CN, string cartellaIn, string cartellaOut)
        {
            string[] files = Directory.GetFiles(cartellaIn,"*.pdf");
            if(files.Length == 0){
                Console.WriteLine(String.Format("Nessun file presente nella cartella: ", cartellaIn));                
                throw new IOException(String.Format("Errore. Nessun file presente nella cartella: \"{0}\"", cartellaIn));
            }
            string FILE_PAG_BIANCA = Path.Combine(CommonHelper.PathProcess, "ini", "bianca.pdf");
            FileInfo filepagBianca = new FileInfo(FILE_PAG_BIANCA);
            if (!filepagBianca.Exists)
            {
                Console.WriteLine(String.Format("File {0} non presente.", FILE_PAG_BIANCA));               
                throw new IOException(String.Format("Errore. File \"{0}\" non presente.", FILE_PAG_BIANCA));
            }
            float _MARGINEDX = 10;
            string _IMGSIGNRESPECT = Path.Combine(CommonHelper.PathProcess, "ini", "imgsignrespect.gif");
            FileInfo fileImgResp = new FileInfo(_IMGSIGNRESPECT);
            if (!fileImgResp.Exists)
            {
                Console.WriteLine(String.Format("File \"{0}\" non presente.", _IMGSIGNRESPECT));
                throw new IOException(String.Format("Errore. File \"{0}\" non presente.", _IMGSIGNRESPECT));
            }
            float _LARGHEZZAZONASEGNI = 22;
            float _MARGINEZONASEGNI = 36;
            float _DISTANZASEGNI = 12;
            int _SpessoreSegni = 2;
            int _InizContatoreBinario = 7; // <------ quando si farà il multithread va rivisto questo parametro 
            bool _FR = true; // <------ se è fronte/retro, per il momento metti false fisso
            ArrayList _Clear = new ArrayList(); // <------ è sempre vuoto
            Hashtable _Inserti = new Hashtable(); // <---------- per ora lascia vuoto
            Hashtable _Fogli = new Hashtable();
            ArrayList ChiudiBusta = new ArrayList();
            float _MARGINESUP = 120;
            float _START = 817;           

            float[] _SEGNI = { 0, 0, 0, 0, 0, 0 }; 
            for (int _i = 0; _i <= 5; _i++)
            {
                _SEGNI[_i] = _START - _MARGINESUP - (_DISTANZASEGNI * _i);
            }

            //string _CN = "00000000";
            bool _InserisciDtmxImbustamento = true;
            bool _InserisciSegniImbustamento = true;


            int inizioUnioneDaPagina_FileDaUnire = 1;
            int numeroDiPagineDaUnire_FileDaUnire = 1;
            int aChePaginaUnire_FilePadre = 1;
            int quantitaPagine_FileDaUnire = 2;
           
            
            //Questo ciclo di inizializzazione dell’array _SEGNI va in base agli altri parametri, quindi mettilo sotto.
            string outputFile = "";
            int numeroFiles = files.Length;
            int numeroFile = 0;
            using (MemoryStream ms = new MemoryStream())
            {

                Document _doc = new Document();
                PdfWriter writer = PdfWriter.GetInstance(_doc, ms);
                writer.PDFXConformance = PdfWriter.PDFA1B;
                excelHelper excel = new excelHelper(Path.Combine(cartellaOut, String.Concat("accompagnamento.xlsx")));
                writer.CreateXmpMetadata();
                int numTotalePagine = 0;
                _doc.Open();
                //stringa serve solo per farlo compilare, cambierà quando il programma sarà in grado di produrre più pdf con nomi diversi
                string destinationPDF = "C:\\appoggio\\out\\12345678\\prova.pdf";
                foreach (var PercorsoFilePdf in files)
                {
                    excel.getFileInfo(PercorsoFilePdf,destinationPDF);
                    _Fogli.Clear();
                    PdfReader reader = new PdfReader(PercorsoFilePdf);
                    PdfReader whitePageReader = new PdfReader(FILE_PAG_BIANCA);
                    int _numpagine = reader.NumberOfPages;
                    numTotalePagine += _numpagine;
                    numeroFile++;
                    //excel.getFileInfo(Path.GetFileName(PercorsoFilePdf), destinationPDF);
                    FileInfo file = new FileInfo(PercorsoFilePdf);
                    if (File.Exists(PercorsoFilePdf))
                    {
                        try
                        {
                            PdfContentByte _writ = writer.DirectContent;
                            PdfImportedPage _page;
                            int _InizContaPagina = 1;
                            int _InizContaBusta = 1;
                            ChiudiBusta.Add(_numpagine);
                            for (int _Pagina = 1; _Pagina <= _numpagine; _Pagina++)
                            {
                                _Fogli.Add(_Pagina, numTotalePagine);
                                int rotazione = reader.GetPageSizeWithRotation(_Pagina).Rotation;
                                _page = writer.GetImportedPage(reader, _Pagina);
                                _doc.NewPage();
                                if (rotazione == 90 || rotazione == 270)
                                {
                                    float rotate = 90;
                                    float x = PageSize.A4.Width;
                                    float y = 0;
                                    float angle = (float)(-rotate * (Math.PI / 180));
                                    float xScale = (float)Math.Cos(angle);
                                    float yScale = (float)Math.Cos(angle);
                                    float xRot = (float)-Math.Sin(angle);
                                    float yRot = (float)Math.Sin(angle);

                                    _writ.AddTemplate(_page, xScale, xRot, yRot, yScale, x, y);
                                }
                                else
                                {
                                    _writ.AddTemplate(_page, 0f, 0f);
                                }
                                if (_Pagina % 2 != 0)
                                    SharedMethods._MettiSegniWriter(ref reader, ref  _writ, _Pagina, ref _InizContaPagina, ChiudiBusta, _SEGNI, _MARGINEDX, _IMGSIGNRESPECT, _LARGHEZZAZONASEGNI, _MARGINEZONASEGNI, _DISTANZASEGNI, _SpessoreSegni, ref _InizContatoreBinario, _FR, ref _InizContaBusta, _CN, _InserisciDtmxImbustamento, _InserisciSegniImbustamento, ref _Clear, ref _Inserti, ref _Fogli);
                                
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Console.WriteLine(String.Format("Errore: {0}", ex.Message));                            
                            throw ex;
                        }
                        finally
                        {
                            reader.Close();
                            GC.Collect();
                        }
               
                    }
                }
                _doc.Close();
                string strNumFile = numeroFile.ToString().PadLeft(8, '0');
                outputFile = Path.Combine(cartellaOut, String.Concat("file_", strNumFile, ".pdf"));
                SharedMethods.MemoryStreamToFile(ms, outputFile);
            }
        }
    }
}
