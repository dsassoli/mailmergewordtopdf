﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Text;
using System.IO;
using System.Windows.Forms;



namespace DLLSharedMethods
{
    public static class CommonHelper
    {
        
        private static string _pathProcess = string.Empty;
        public static string PathProcess
        {
            get
            {
                if (_pathProcess != null)
                {
                    if (_pathProcess != string.Empty)
                    {
                        return _pathProcess;
                    }
                    else
                    {
                        try
                        {
                            _pathProcess = ConfigurationManager.AppSettings["PathAppoggio"].ToString();
                        }
                        catch (Exception ex)
                        {  // se la chiave non esiste...
                            Console.WriteLine("Errore di:" + ex.Message.ToString());
                            _pathProcess = null;

                            string fileLog = Application.StartupPath + @"\logApplication.txt";

                            using (StreamWriter sw = new StreamWriter(fileLog, true))
                            {
                                sw.Write("******************** " + DateTime.Now);
                                sw.WriteLine("********************");
                                if (ex.InnerException != null)
                                {
                                    sw.Write("Inner Exception Type: ");
                                    sw.WriteLine(ex.InnerException.GetType().ToString());
                                    sw.Write("Inner Exception: ");
                                    sw.WriteLine(ex.InnerException.Message);
                                    sw.Write("Inner Source: ");
                                    sw.WriteLine(ex.InnerException.Source);
                                    if (ex.InnerException.StackTrace != null)
                                        sw.WriteLine("Inner Stack Trace: ");
                                    sw.WriteLine(ex.InnerException.StackTrace);
                                }
                                sw.Write("Exception Type: ");
                                sw.WriteLine(ex.GetType().ToString());
                                sw.WriteLine("Exception: " + ex.Message);
                                sw.WriteLine("Stack Trace: ");
                                if (ex.StackTrace != null)
                                    sw.WriteLine(ex.StackTrace);
                                sw.WriteLine();
                                sw.WriteLine("*************************************************************");
                                
                            }

                            throw;
                        }
                        
                    }
                }
                return _pathProcess;
            }
        }
    }
}
